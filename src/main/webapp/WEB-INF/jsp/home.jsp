<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>


<html lang="${lang}">
<head>
    <title>Menu</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb shadow">
            <c:if test="${login != null}">
                <li class="breadcrumb-item"><a href="profile"><fmt:message key="home_jsp.list.profile"/></a></li>
            </c:if>
            <c:if test="${login != null}">
                <li class="breadcrumb-item"><a href="request"><fmt:message key="home_jsp.list.orders"/></a></li>
            </c:if>
            <c:if test="${login == null}">
                <li class="breadcrumb-item"><a href="registration"><fmt:message key="home_jsp.list.registration"/></a>
                </li>
            </c:if>
            <c:if test="${login != null}">
                <li class="breadcrumb-item"><a href="adminPanel"><fmt:message key="home_jsp.list.user_list"/></a></li>
            </c:if>
            <c:if test="${login != null}">
                <li class="breadcrumb-item"><a href="logout"><fmt:message key="home_jsp.list.logout"/></a></li>
            </c:if>
            <c:if test="${login == null}">
                <li class="breadcrumb-item"><a href="login"><fmt:message key="home_jsp.list.login"/></a></li>
            </c:if>
            <li class="breadcrumb-item"><a href="order"><fmt:message key="home_jsp.list.create_order"/></a></li>
            <c:if test="${isAdmin != null}">
                <li class="breadcrumb-item"><a href="orderList"><fmt:message key="home_jsp.list.order_list"/></a></li>
            </c:if>
        </ol>
        <div class="dropdown">
            <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <fmt:message key="home_jsp.language"/>
            </button>
            <form action="home" method="post">
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <input type="submit" class="btn btn-light dropdown-item" name="lang" value="ru">
                    <input type="submit" class="btn btn-light dropdown-item" name="lang" value="en">
                </div>
            </form>
        </div>
    </nav>

    <hr>
    <c:if test="${login != null}">
        <div>
            <div><fmt:message key="home_jsp.logged_as"/></div>
            <div><c:out value="${login}"/></div>
        </div>
    </c:if>
</div>
</body>
</html>
