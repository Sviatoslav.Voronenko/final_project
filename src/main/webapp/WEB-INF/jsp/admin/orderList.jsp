<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<!doctype html>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script type="text/javascript" src="resources/js/changeDate.js"></script>
    <script type="text/javascript" src="resources/js/tableFilter.js"></script>
</head>
<body>
<div class="container">
    <select class="btn btn-info dropdown-toggle" id="myInput" onchange="findByStatus(5)">
        <c:forEach var="status" items="${statuses}">
            <option>
                <c:out value="${status.getName()}"/>
            </option>
        </c:forEach>
    </select>
    <button class="btn btn-outline-danger" onclick="reset()"><fmt:message key="requests_jsp.btn.reset"/></button>
    <button class="btn btn-outline-primary"><a href="/home"><fmt:message key="order_list_jsp.btn.back"/></a></button>
    <table class="table table-bordered" id="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">id</th>
            <th scope="col"><fmt:message key="requests_jsp.table.head.departure"/></th>
            <th scope="col"><fmt:message key="requests_jsp.table.head.receiving"/></th>
            <th scope="col"><fmt:message key="requests_jsp.table.head.from_the_city"/></th>
            <th scope="col"><fmt:message key="requests_jsp.table.head.to_the_city"/></th>
            <th scope="col"><fmt:message key="requests_jsp.table.head.status"/></th>
            <th scope="col"><fmt:message key="order_jsp.form.tariff"/></th>
            <th scope="col"><fmt:message key="requests_jsp.date_of_creation"/></th>
            <th scope="col"><fmt:message key="requests_jsp.table.head.cost"/></th>
            <th scope="col"><fmt:message key="requests_jsp.table.head.user"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="request" items="${requests}">
            <tr>
                <td><a href="request?id=${request.getId()}"><c:out value="${request.getId()}"/></a></td>
                <td>
                    <c:choose>
                        <c:when test="${request.getDeparture() == 'null'}">
                            <fmt:message key="requests_jsp.list.not_assigned"/>
                        </c:when>
                        <c:otherwise>
                            <c:out value="${request.getDeparture()}"/>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <c:choose>
                        <c:when test="${request.getReceiving() == 'null'}">
                            <fmt:message key="requests_jsp.list.not_assigned"/>
                        </c:when>
                        <c:otherwise>
                            <c:out value="${request.getReceiving()}"/>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td><c:out value="${request.getCityFromName()}"/></td>
                <td><c:out value="${request.getCityToName()}"/></td>
                <td><c:out value="${request.getStatusName()}"/></td>
                <td><c:out value="${request.getTariffName()}"/></td>
                <td><c:out value="${request.getDateOfCreation()}"/></td>
                <td><c:out value="${request.getCost()}"/></td>
                <td><a href="/user?id=${request.getUserId()}" id="request_userFirstName"><c:out
                        value="${request.getUserFirstName()}"/></a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>