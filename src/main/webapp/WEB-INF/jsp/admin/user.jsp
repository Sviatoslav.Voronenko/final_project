<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>

<!doctype html>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">id</th>
            <th scope="col"><fmt:message key="user_jsp.user.first_name"/></th>
            <th scope="col"><fmt:message key="user_jsp.user.last_name"/></th>
            <th scope="col"><fmt:message key="user_jsp.user.email"/></th>
            <th scope="col"><fmt:message key="user_jsp.user.phone"/></th>
            <th scope="col"><fmt:message key="user_jsp.user.role"/></th>
            <th scope="col"><fmt:message key="user_jsp.user.password"/></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row"><c:out value="${userProfile.getId()}"/></th>
            <td><c:out value="${userProfile.getFirstName()}"/></td>
            <td><c:out value="${userProfile.getLastName()}"/></td>
            <td><c:out value="${userProfile.getEmail()}"/></td>
            <td><c:out value="${userProfile.getPhone()}"/></td>
            <td><c:out value="${userProfile.getRole().getName()}"/></td>
            <td><c:out value="${userProfile.getPassword()}"/></td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>