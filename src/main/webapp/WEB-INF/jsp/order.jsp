<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<!doctype html>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="resources/css/style.css">
    <script src="resources/js/calc.js"></script>
</head>
<body>
<div class="container">
    <form action="order" method="post">

        <div class="ok">
            <h2><fmt:message key="order_jsp.form.cargo_title"/></h2>
            <div>
                <label for="depth"><fmt:message key="order_jsp.form.depth"/></label>
                <input type="text" name="depth" id="depth" required>
            </div>
            <div>
                <label for="width"><fmt:message key="order_jsp.form.width"/></label>
                <input type="text" name="width" id="width" required>
            </div>
            <div>
                <label for="height"><fmt:message key="order_jsp.form.height"/></label>
                <input type="text" name="height" id="height" required>
            </div>
            <div>
                <label for="weight"><fmt:message key="order_jsp.form.weight"/></label>
                <input type="text" name="weight" id="weight" required>
            </div>
        </div>
        <div>
            <label for="cargoType"><fmt:message key="order_jsp.form.cargoType"/></label>
            <select name="cargoType" id="cargoType">
                <c:forEach var="cargotype" items="${cargotypes}">
                    <option>
                        <c:out value="${cargotype.getName()}"/>
                    </option>
                </c:forEach>
            </select>
        </div>
        <div>
            <label for="cityFrom"><fmt:message key="order_jsp.form.city_from"/></label>
            <select name="cityFrom" id="cityFrom" onchange="check()">
                <c:forEach var="city" items="${cities}">
                    <option value="${city.getId()}">
                        <c:out value="${city.getName()}"/>
                    </option>
                </c:forEach>
            </select>
        </div>
        <div>
            <label for="cityTo"><fmt:message key="order_jsp.form.city_to"/></label>
            <select name="cityTo" id="cityTo" onchange="check()">
                <c:forEach var="city" items="${cities}">
                    <option value="${city.getId()}">
                        <c:out value="${city.getName()}"/>
                    </option>
                </c:forEach>
            </select>
        </div>
        <c:if test="${login != null}">
            <div>
                <label for="address"><fmt:message key="order_jsp.form.address"/></label>
                <input type="text" name="address" id="address">
            </div>
        </c:if>
        <div>
            <div class="d-inline-flex bd-highlight">
                <label for="tariff"><fmt:message key="order_jsp.form.tariff"/></label>
                <div id="current_tariff" class="pl-2"></div>
            </div>
            <select name="tariff" id="tariff" onchange="check()" hidden>
                <c:forEach var="tariff" items="${tariffs}">
                    <option value="${tariff.getId()}">
                        <c:out value="${tariff.getName()}"/>
                    </option>
                </c:forEach>
            </select>
        </div>
        <input type="button" value="<fmt:message key="order_jsp.form.calc_cost"/>" onclick="calcTotalCost()">
        <input type="hidden" id="resultForm" name="cost">
        <div id="result"></div>
        <c:if test="${login != null}">
            <input type="submit" value="<fmt:message key="order_jsp.form.send_req"/>">
        </c:if>
    </form>
</div>
</body>
</html>