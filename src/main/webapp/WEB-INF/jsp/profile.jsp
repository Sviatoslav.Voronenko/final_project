<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

</head>
<body>
<div class="container">
    <div>
        <div class="d-flex bd-highlight shadow p-3 mb-5 bg-white rounded">
            <label for="first_name"><fmt:message key="user_jsp.user.first_name"/>:</label>
            <div id="first_name" class="pl-2">
                <c:out value="${user.getFirst_name()}"/>
            </div>
        </div>
        <div class="d-flex bd-highlight shadow p-3 mb-5 bg-white rounded">
            <label for="last_name"><fmt:message key="user_jsp.user.last_name"/>:</label>
            <div id="last_name" class="pl-2">
                <c:out value="${user.getLast_name()}"/>
            </div>
        </div>
        <div class="d-flex bd-highlight shadow p-3 mb-5 bg-white rounded">
            <label for="email"><fmt:message key="user_jsp.user.email"/>:</label>
            <div id="email" class="pl-2">
                <c:out value="${user.getEmail()}"/>
            </div>
        </div>
        <div class="d-flex bd-highlight shadow p-3 mb-5 bg-white rounded">
            <label for="phone"><fmt:message key="user_jsp.user.phone"/>:</label>
            <div id="phone" class="pl-2">
                <c:out value="${user.getPhone()}"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>