<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

</head>
<body>
<div class="container">
    <div>
        <h1><fmt:message key="orderComplete_jsp.main_thank"/></h1>
        <a href="/home"><fmt:message key="orderComplete_jsp.main_back"/> </a>
    </div>
</div>
</body>
</html>