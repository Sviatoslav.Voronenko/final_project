<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<link rel="stylesheet" href="resources/css/style.css">
<script type="text/javascript" src="resources/js/changeDate.js"></script>
</head>
<body>
<div class="container">
    <div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col"><fmt:message key="requests_jsp.table.head.departure"/></th>
                <th scope="col"><fmt:message key="requests_jsp.table.head.receiving"/></th>
                <th scope="col"><fmt:message key="requests_jsp.table.head.address"/></th>
                <th scope="col"><fmt:message key="requests_jsp.table.head.status"/></th>
                <th scope="col"><fmt:message key="requests_jsp.table.head.from_the_city"/></th>
                <th scope="col"><fmt:message key="requests_jsp.table.head.to_the_city"/></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><a href="/request?id=${request.getId()}"><c:out value="${request.getId()}"/></a></td>
                <td>
                    <c:choose>
                        <c:when test="${request.getReceiving() == 'null'}">
                            <fmt:message key="requests_jsp.list.not_assigned"/>
                        </c:when>
                        <c:otherwise>
                            <c:out value="${request.getDeparture()}"/>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <c:choose>
                        <c:when test="${request.getReceiving() == 'null'}">
                            <fmt:message key="requests_jsp.list.not_assigned"/>
                        </c:when>
                        <c:otherwise>
                            <c:out value="${request.getReceiving()}"/>
                        </c:otherwise>
                    </c:choose>
                </td>

                <td>
                    <c:choose>
                        <c:when test="${empty request.getAddress()}">
                            <fmt:message key="requests_jsp.list.address"/>
                        </c:when>
                        <c:otherwise>
                            <c:out value="${request.getAddress()}"/>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <c:out value="${request.getStatusName()}"/>
                </td>
                <td>
                    <c:out value="${request.getCityFrom()}"/>
                </td>
                <td>
                    <c:out value="${request.getCityTo()}"/>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col"><fmt:message key="requests_jsp.table.head.cost"/></th>
            <th scope="col"><fmt:message key="requests_jsp.date_of_creation"/></th>
            <th scope="col"><fmt:message key="requests_jsp.date_of_payment"/></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><c:out value="${request.getCost()}"/></td>
            <td><c:out value="${request.getDateOfCreation()}"/></td>
            <td><c:out value="${request.getDateOfPayment()}"/></td>
        </tr>
        </tbody>
    </table>
    <h2><fmt:message key="order_jsp.form.cargo_title"/></h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col"><fmt:message key="requests_jsp.cargo_depth"/></th>
            <th scope="col"><fmt:message key="requests_jsp.cargo_height"/></th>
            <th scope="col"><fmt:message key="requests_jsp.cargo_width"/></th>
            <th scope="col"><fmt:message key="requests_jsp.cargo_weight"/></th>
            <th scope="col"><fmt:message key="requests_jsp.cargo_type"/></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><c:out value="${cargo.getDepth()}"/></td>
            <td><c:out value="${cargo.getHeight()}"/></td>
            <td><c:out value="${cargo.getWidth()}"/></td>
            <td><c:out value="${cargo.getWeight()}"/></td>
            <td><c:out value="${cargo.getCargoType().getName()}"/></td>
        </tr>
        </tbody>
    </table>
    <c:if test="${request.getStatusId() == '2'}">
    <form action="request" method="post">
        <input type="hidden" id="hidden_request_id" name="request_id" value="${request.getId()}">
        <input type="submit" value="<fmt:message key="requests_jsp.form.pay"/> ">
    </form>
    </c:if>
    <c:if test="${isAdmin != null}">
    <div>
        <input type="button" value="<fmt:message key="order_list_jsp.button.submit"/>   " id="editBtn_"
               class="editBtn_${request.getId()}" onclick="changeDate(${request.getId()})">
    </div>
    <div>
        <form action="orderList" method="post" id="formPlace" class="formPlace_${request.getId()}">
            <input type="hidden" name="requestId" value="${request.getId()}">
            <input type="hidden" name="oldDeparture" value="${request.getDeparture()}">
            <input type="hidden" name="oldReceiving" value="${request.getReceiving()}">
            <select name="status" id="" class="status_for_edit_${request.getId()}" hidden>
                <c:forEach var="status" items="${statuses}">
                    <option>
                        <c:out value="${status.getName()}"/>
                    </option>
                </c:forEach>
            </select>
        </form>
        </c:if>
    </div>

</body>
</html>