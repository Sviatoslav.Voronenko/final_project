<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>


<link rel="stylesheet" href="<c:url value="resources/css/style.css"/>">
</head>
<body>
<div class="container">

    <form action="login" method="post" class="oke col-md-6 offset-md-3 mt-3">
        <div class="input-group flex-nowrap">
            <div class="input-group-prepend">
                <span class="input-group-text">@</span>
            </div>
            <input type="text" name="name" id="login" class="form-control"
                   placeholder="<fmt:message key="login_jsp.form.email"/>" aria-label="Username"
                   aria-describedby="addon-wrapping">
        </div>
        <div class="input-group flex-nowrap">
            <input type="password" name="password" id="password" class="form-control"
                   placeholder="<fmt:message key="login_jsp.form.password"/>" aria-label="Username"
                   aria-describedby="addon-wrapping">
            <div class="input-group-prepend">
                <span class="input-group-text">&</span>
            </div>
        </div>
        <input type="submit" value="login" class="btn btn-success mt-1">
    </form>
</div>
</body>
</html>