<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="resources/js/registrationAndValidationEmail.js">
</script>
</head>
<body>

<div class="container">
    <form action="registration" method="post" enctype="multipart/form-data" id="form" name="reg" class="formCl">

        <label for="name"><fmt:message key="registation_jsp.form_name"/></label>
        <input type="text" id="name" name="name"><br>

        <label for="email"><fmt:message key="registation_jsp.form_email"/></label>
        <input type="email" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"><br>
        <div id="result"></div>

        <label for="phone"><fmt:message key="registation_jsp.form_phome"/></label>
        <input type="phone" id="phone" name="phone"><br>

        <label for="password"><fmt:message key="registation_jsp.form_password"/></label>
        <input type="password" id="password" name="password"><br>

    </form>
    <button onclick="sendReq()"><fmt:message key="registation_jsp.form_confirm"/></button>
    <div id="resp1100" hidden><fmt:message key="registation_jsp.status.1100"/></div>
    <div id="resp1101" hidden><fmt:message key="registation_jsp.status.1101"/></div>
</div>

</body>
</html>