<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

</head>
<body>
<h1><fmt:message key="ThanksForOrder_jsp.thanks"/></h1>
<h3><fmt:message key="ThanksForOrder_jsp.resp_soon"/></h3>
</body>
</html>