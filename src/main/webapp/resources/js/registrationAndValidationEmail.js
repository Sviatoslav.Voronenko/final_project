function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validate() {
    const $result = $("#result");
    const email = $("#email").val();
    $result.text("");

    if (validateEmail(email)) {
        $result.text(email + " is valid email");
        $result.css("color", "green");
        return true;
    } else {
        $result.text(email + " is not valid email");
        $result.css("color", "red");
        return false;
    }
}


function isEmpty(str) {
    if (str.trim() == '')
        return true;

    return false;
}

function sendReq() {
    // Создаем экземпляр класса XMLHttpRequest
    const request = new XMLHttpRequest();

    // Указываем путь до файла на сервере, который будет обрабатывать наш запрос
    const url = "/registration";
    var email = document.getElementById("email").value;
    var name = document.getElementById("name").value;
    var phone = document.getElementById("phone").value;
    var password = document.getElementById("password").value;
    if (!validate()) {
        return;
    }
    if (isEmpty(name) || isEmpty(email) || isEmpty(phone) || isEmpty(password)) {
        alert(document.getElementById("resp1101").innerText);
        return;
    }
    const params = "email=" + email + "&name=" + name + "&phone=" + phone + "&password=" + password;

    request.open("POST", url, true);

    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    request.addEventListener("readystatechange", () => {

        if (request.readyState === 4 && request.status === 200) {

        }
    });

    request.send(params);
    request.onload = function () {
        if (request.status == 200) {
            location.pathname = "home";
        }
        if (request.status == 1100) {
            alert(document.getElementById("resp1100").innerText);
        }
        if (request.status == 1101) {
            alert(document.getElementById("resp1101").innerText);
        }
    }
}