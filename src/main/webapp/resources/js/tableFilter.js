function findByStatus(cells) {
    var selec = document.getElementById("myInput");
    var table = document.getElementById("table");
    for (var i = 1; i < table.rows.length; i++) {
        if (selec.value == table.rows[i].cells[cells].innerText.trim()) {
            table.rows[i].style.display = "";
        } else {
            table.rows[i].style.display = "none";
        }
    }
}

function reset() {
    var table = document.getElementById("table");
    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].style.display = "";
    }
}