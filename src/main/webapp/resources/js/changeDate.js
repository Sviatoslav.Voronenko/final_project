function changeDate(id) {
    var editBtnId = ".editBtn_" + id;
    var formPlaceName = ".formPlace_" + id;
    var statusForEdit = ".status_for_edit_" + id;

    var formPlace = document.querySelector(formPlaceName);
    var div = document.createElement('input');
    div.setAttribute("type", "datetime-local");
    div.setAttribute("name", "departure_date_new");
    formPlace.append(div)

    var inputRec = document.createElement('input');
    inputRec.setAttribute("type", "datetime-local");
    inputRec.setAttribute("name", "receiving_date_new");
    formPlace.append(inputRec);

    var btnSubmit = document.createElement('input');
    btnSubmit.setAttribute("type", "submit")
    btnSubmit.setAttribute("value", "Submit");
    formPlace.append(btnSubmit);

    var statusEdit = document.querySelector(statusForEdit);
    statusEdit.removeAttribute('hidden');


    var editBtn = document.querySelector(editBtnId);
    editBtn.disabled = true;

}