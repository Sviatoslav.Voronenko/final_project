window.onload = function () {
    let tariff = document.getElementById("tariff");
    document.getElementById("current_tariff").innerText = tariff.options[tariff.options.selectedIndex].innerText.trim();
}

function calcTotalCost() {
    // Creating the XMLHttpRequest object
    var request = new XMLHttpRequest();

    // Instantiating the request object
    request.open("GET", "/order");

    // Defining event listener for readystatechange event
    request.onreadystatechange = function () {
        // Check if the request is compete and was successful
        if (this.readyState === 4 && this.status === 200) {
            let depth = document.getElementById("depth").value;
            let width = document.getElementById("width").value;
            let height = document.getElementById("height").value;
            let weigth = document.getElementById("weight").value;
            let volume = depth * width * height;
            let result = volume * 0.1 + (weigth / 20);
            document.getElementById("result").innerHTML = parseInt(result * 100) / 100 + calcTariff();
            document.getElementById("resultForm").value = parseInt(result * 100) / 100 + calcTariff();
        }
    };

    // Sending the request to the server
    request.send();
}

function calcTariff() {
    var tariff = document.getElementById("tariff");
    if (tariff.value == tariff.options[0].value) {
        return 35;
    } else {
        return 65;
    }
}

function check() {
    var cityFrom = document.getElementById("cityFrom").value;
    var cityTo = document.getElementById("cityTo").value;
    var tariff = document.getElementById("tariff");
    if (cityFrom == cityTo) {
        tariff.value = 1;
    } else {
        tariff.value = 2;
    }
    console.log(tariff);
    const result = tariff.options[tariff.options.selectedIndex].innerText.trim();
    document.getElementById("current_tariff").innerText = result;
}