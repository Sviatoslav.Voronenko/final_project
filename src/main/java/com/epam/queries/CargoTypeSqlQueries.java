package com.epam.queries;

public class CargoTypeSqlQueries {
    public static final String SELECT_FROM_CARGOTYPE_WHERE_ID = "select * from cargotype where id = ?";
    public static final String SELECT_FROM_CARGOTYPE_TRANSLATE = "SELECT cl.cargotype_id as id, cl.name as name FROM cargotype_has_languages as cl\n" +
            "join languages as l on cl.languages_id = l.id\n" +
            "where cl.cargotype_id = ? and l.lang_code = ?";
    public static final String SELECT_FROM_CARGOTYPE = "select * from cargotype";
    public static final String SELECT_FROM_CARGOTYPES_TRANSLATE = "SELECT cl.cargotype_id as id, cl.name as name FROM cargotype_has_languages as cl\n" +
            "join languages as l on cl.languages_id = l.id\n" +
            "where l.lang_code = ?";
    public static final String INSERT_INTO_CARGOTYPE_NAME_VALUES = "insert into cargotype(name) values (?)";
}
