package com.epam.queries;

public class CargoSqlQueries {
    public static final String SELECT_FROM_CARGO_WHERE_ID = "SELECT c.*, ct.name as cargotype_name FROM cargo as c" +
            " join cargotype as ct on c.cargotype_id = ct.id" +
            " where c.id = ?";
    public static final String SELECT_FROM_CARGO_TRANSLATE_WHERE_ID = "SELECT c.*, cl.name as cargotype_name FROM cargo as c\n" +
            "join cargotype_has_languages as cl on c.cargotype_id = cl.cargotype_id\n" +
            "join languages as lang on cl.languages_id = lang.id\n" +
            "where c.id = ? and lang.lang_code = ?";
    public static final String SELECT_ALL_FROM_CARGO_TRANSLATE = "SELECT c.*, cl.name as cargotype_name FROM cargo as c\n" +
            "join cargotype_has_languages as cl on c.cargotype_id = cl.cargotype_id\n" +
            "join languages as lang on cl.languages_id = lang.id\n" +
            "where lang.lang_code = ?";
    public static final String SELECT_ALL_FROM_CARGO = "SELECT c.*, ct.name as cargotype_name FROM cargo as c\n" +
            "join cargotype as ct on c.cargotype_id = ct.id";

    public static final String INSERT_INTO_CARGO_CARGOTYPE_ID_WEIGHT_HEIGHT_WIDTH_DEPTH_REQUEST_ID_VALUES = "insert into cargo (cargotype_id, weight, height, width, depth, request_id) values (?,?,?,?,?,?)";
    public static final String SELECT_FROM_CARGO_BY_REQUEST_ID = "SELECT c.*, ct.name as cargotype_name FROM cargo as c\n" +
            "join cargotype as ct on c.cargotype_id = ct.id\n" +
            "where c.request_id = ?";
    public static final String SELECT_FROM_CARGO_BY_REQUEST_ID_TRANSLATE = "SELECT c.*, cl.name as cargotype_name FROM cargo as c\n" +
            "join cargotype_has_languages as cl on c.cargotype_id = cl.cargotype_id\n" +
            "join languages as lang on cl.languages_id = lang.id\n" +
            "where c.request_id = ? and lang.lang_code = ?";
}
