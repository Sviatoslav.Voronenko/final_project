package com.epam.queries;

public class RoleSqlQueries {
    public static final String SELECT_FROM_ROLE_WHERE_ID = "select * from role where id = ?";
    public static final String SELECT_FROM_ROLE = "select * from role";
    public static final String INSERT_INTO_ROLE_NAME_VALUES = "insert into role (name) values (?)";
    public static final String DELETE_FROM_ROLE_WHERE_ID = "delete from role where id = ?";
    public static final String SELECT_FROM_ROLE_WHERE_NAME = "select * from role where name = ?";
}
