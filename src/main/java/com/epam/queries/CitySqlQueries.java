package com.epam.queries;

public class CitySqlQueries {
    public static final String SELECT_FROM_CITY_WHERE_ID = "select * from city where id = ?";
    public static final String SELECT_FROM_CITY = "select * from city";
    public static final String SELECT_FROM_CITY_TRANSLATE = "SELECT cl.city_id as id, cl.name as name FROM delivery.city_has_languages as cl" +
            " join languages as lang on cl.languages_id = lang.id" +
            " where cl.city_id = ?  and" +
            " lang.lang_code = ?";
    public static final String SELECT_ALL_FROM_CITIES_TRANSLATE = "SELECT cl.city_id as id, cl.name as name FROM delivery.city_has_languages as cl" +
            " join languages as lang on cl.languages_id = lang.id" +
            " where lang.lang_code = ?";
    public static final String INSERT_INTO_CITY_NAME_VALUES = "insert into city (name) values (?)";
    public static final String DELETE_FROM_CITY_WHERE_ID = "delete from city where id = ?";
}
