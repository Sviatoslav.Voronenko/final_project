package com.epam.exception;

public class Messages {

    public Messages() {
    }

    public static final String ERR_CANNOT_OBTAIN_USERS = "Cannot obtain a users";
    public static final String ERR_CANNOT_OBTAIN_CITIES = "Cannot obtain a cities";
    public static final String ERR_CANNOT_OBTAIN_USER_BY_ID = "Cannot obtain a user by its id";
    public static final String ERR_CANNOT_OBTAIN_CITY_BY_ID = "Cannot obtain a city by its id";
}
