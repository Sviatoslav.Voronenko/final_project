package com.epam.service.interfaces;

import com.epam.entities.Role;

import java.util.List;

public interface RoleService {

    Role findById(Long id);

    Role findByName(String name);

    List<Role> findAll();

    Role create(String name);

    Role update(Role role, Long id);

    void deleteById(Long id);

    void delete(String name);
}
