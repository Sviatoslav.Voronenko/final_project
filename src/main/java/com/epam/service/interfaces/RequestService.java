package com.epam.service.interfaces;

import com.epam.dto.*;
import com.epam.entities.Request;

import java.util.List;

public interface RequestService {
    Request createRequestByUser(RequestByUserDto requestByUserDto);

    List<Request> findAll(String lang);

    RequestDto findByIdAll(Long id, String lang);

    Request findById(Long id, String lang);

    List<RequestForUserProfileDto> findAllUserRequests(Long id, String lang);

    List<RequestForUserProfileDto> findAllUserRequests(String lang);

    Boolean updateByManager(String departure, String receiving, String statusId, String requestId);

    List<RequestForManagerDto> findAllForManager(String lang);

    Boolean updateToPaidStatus(RequestUpdateStatusDto requestUpdateStatusDto);
}
