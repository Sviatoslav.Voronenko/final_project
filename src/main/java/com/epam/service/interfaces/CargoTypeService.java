package com.epam.service.interfaces;

import com.epam.entities.CargoType;

import java.util.List;

public interface CargoTypeService {
    CargoType findById(Long id, String lang);

    List<CargoType> findAll(String lang);

    CargoType create(String name);

    void deleteById(Long id);
}
