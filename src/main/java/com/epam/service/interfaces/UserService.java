package com.epam.service.interfaces;

import com.epam.entities.User;

import java.util.List;

public interface UserService {
    User findById(Long id);

    User findByEmail(String email);

    List<User> findAll();

    User create(User user);
}
