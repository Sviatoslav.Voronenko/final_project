package com.epam.service.interfaces;

import com.epam.dto.CargoDto;
import com.epam.entities.Cargo;

import java.util.List;

public interface CargoService {
    Cargo findById(Long id, String lang);

    List<Cargo> findAll(String lang);

    Cargo create(CargoDto cargoDto);

    CargoDto findByRequestId(Long id, String lang);

    void deleteById(Long id);
}
