package com.epam.service.interfaces;

import java.util.List;

public interface LanguageService {

    Long getIdByLangCode(String lang);

    List<String> findAllLangCodes();
}
