package com.epam.service.impl;

import com.epam.dao.impl.RoleDAOImpl;
import com.epam.dao.interfaces.RoleDAO;
import com.epam.entities.Role;
import com.epam.service.interfaces.RoleService;

import java.util.List;
import java.util.Optional;

public class RoleServiceImpl implements RoleService {
    RoleDAO roleDAO = new RoleDAOImpl();

    @Override
    public Role findById(Long id) {
        return roleDAO.findById(id).orElseThrow(() -> new RuntimeException());
    }

    @Override
    public List<Role> findAll() {
        return roleDAO.findAll();
    }

    @Override
    public Role create(String name) {
        Role role = new Role();
        role.setName(name);
        return roleDAO.create(role);
    }

    @Override
    public Role findByName(String name) {
        return roleDAO.findByName(name).orElseThrow(RuntimeException::new);
    }

    @Override
    public Role update(Role role, Long id) {
        return null;
    }


    @Override
    public void deleteById(Long id) {
        roleDAO.deleteById(id);
    }

    @Override
    public void delete(String name) {
        Role role = new Role();
        role.setName(name);
        roleDAO.delete(role);
    }
}
