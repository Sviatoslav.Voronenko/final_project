package com.epam.service.impl;

import com.epam.dao.impl.TariffDAOImpl;
import com.epam.dao.interfaces.TariffDAO;
import com.epam.entities.Tariff;
import com.epam.service.interfaces.TariffService;

import java.util.List;

public class TariffServiceImpl implements TariffService {

    private TariffDAO tariffDAO = new TariffDAOImpl();

    @Override
    public Tariff findById(Long id, String lang) {
        return tariffDAO.findById(id, lang).orElseThrow(RuntimeException::new);
    }

    @Override
    public List<Tariff> findAll(String lang) {
        return tariffDAO.findAll(lang);
    }

    @Override
    public Tariff create(String name) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }
}
