package com.epam.service.impl;

import com.epam.dao.impl.StatusDAOImpl;
import com.epam.dao.interfaces.StatusDAO;
import com.epam.entities.Status;
import com.epam.service.interfaces.StatusService;

import java.util.List;

public class StatusServiceImpl implements StatusService {

    private StatusDAO statusDAO = new StatusDAOImpl();

    @Override
    public Status findById(Long id, String lang) {
        return statusDAO.findById(id, lang).orElseThrow(RuntimeException::new);
    }

    @Override
    public List<Status> findAll(String lang) {
        return statusDAO.findAll(lang);
    }

    @Override
    public Status create(String name) {
        Status status = new Status();
        status.setName(name);
        return statusDAO.create(status);
    }

    @Override
    public void deleteById(Long id) {

    }
}
