package com.epam.service.impl;

import com.epam.dao.impl.LanguageDAOImpl;
import com.epam.service.interfaces.LanguageService;

import java.util.List;

public class LanguageServiceImpl implements LanguageService {
    private final LanguageDAOImpl languageDAO = new LanguageDAOImpl();

    @Override
    public Long getIdByLangCode(String lang) {
        return languageDAO.getIdByLangCode(lang);
    }

    @Override
    public List<String> findAllLangCodes() {
        return languageDAO.findAllLangCodes();
    }
}
