package com.epam.service.impl;

import com.epam.dao.impl.CityDAOImpl;
import com.epam.dao.interfaces.CityDAO;
import com.epam.entities.City;
import com.epam.service.interfaces.CityService;

import java.util.List;

public class CityServiceImpl implements CityService {
    CityDAO cityDAO = new CityDAOImpl();

    @Override
    public City findById(Long id, String lang) {
        return cityDAO.findById(id, lang).orElseThrow(RuntimeException::new);
    }

    @Override
    public List<City> findAll(String lang) {
        return cityDAO.findAll(lang);
    }

    @Override
    public City create(String name) {
        City city = new City();
        city.setName(name);
        return cityDAO.create(city);
    }

    @Override
    public void deleteById(Long id) {
        cityDAO.deleteById(id);
    }
}
