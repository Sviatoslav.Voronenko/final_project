package com.epam.service.impl;

import com.epam.dao.impl.UserDAOImpl;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;
import com.epam.service.interfaces.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDAO userDAO = new UserDAOImpl();

    @Override
    public User findById(Long id) {
        return userDAO.findById(id).orElseThrow(() -> new RuntimeException());
    }

    @Override
    public User findByEmail(String email) {
        return userDAO.findByEmail(email).orElse(null);
    }

    @Override
    public List<User> findAll() {
        return userDAO.findAll();
    }

    @Override
    public User create(User user) {
        return userDAO.create(user);
    }
}
