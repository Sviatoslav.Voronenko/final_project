package com.epam.service.impl;

import com.epam.dao.impl.CargoDAOImpl;
import com.epam.dao.interfaces.CargoDAO;
import com.epam.dto.CargoDto;
import com.epam.entities.Cargo;
import com.epam.service.interfaces.CargoService;
import com.epam.service.interfaces.RequestService;

import java.util.List;

public class CargoServiceImpl implements CargoService {
    private CargoDAO cargoDAO = new CargoDAOImpl();

    private RequestService requestService = new RequestServiceImpl();

    @Override
    public Cargo findById(Long id, String lang) {
        return cargoDAO.findById(id, lang).orElse(null);
    }

    @Override
    public List<Cargo> findAll(String lang) {
        return cargoDAO.findAll(lang);
    }

    @Override
    public Cargo create(CargoDto cargoDto) {
        return cargoDAO.create(Cargo
                .builder()
                .weight(cargoDto.getWeight())
                .width(cargoDto.getWidth())
                .depth(cargoDto.getDepth())
                .height(cargoDto.getHeight())
                .cargoType(cargoDto.getCargoType())
                .requestId(cargoDto.getRequestId())
                .build());
    }

    @Override
    public CargoDto findByRequestId(Long id, String lang) {
        Cargo byRequestId = cargoDAO.findByRequestId(id, lang).orElse(null);
        if (byRequestId == null) {
            return null;
        }
        CargoDto cargoDto = CargoDto
                .builder()
                .width(byRequestId.getWidth())
                .height(byRequestId.getHeight())
                .depth(byRequestId.getDepth())
                .weight(byRequestId.getWeight())
                .cargoType(byRequestId.getCargoType())
                .requestId(requestService.findById(byRequestId.getRequestId().getId(), lang))
                .build();
        return cargoDto;
    }

    @Override
    public void deleteById(Long id) {

    }
}
