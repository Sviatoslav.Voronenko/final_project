package com.epam.service.impl;

import com.epam.dao.impl.RequestDAOImpl;
import com.epam.dao.interfaces.RequestDAO;
import com.epam.dto.*;
import com.epam.entities.City;
import com.epam.entities.Request;
import com.epam.entities.Tariff;
import com.epam.service.interfaces.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class RequestServiceImpl implements RequestService {

    private RequestDAO requestDAO = new RequestDAOImpl();

    private StatusService statusService = new StatusServiceImpl();

    private CityService cityService = new CityServiceImpl();

    private TariffService tariffService = new TariffServiceImpl();

    private CargoTypeService cargoTypeService = new CargoTypeServiceImpl();


    @Override
    public RequestDto findByIdAll(Long id, String lang) {
        Request request = requestDAO.findById(id, lang).orElse(null);
        System.out.println(request);
        return RequestDto
                .builder()
                .id(request.getId())
                .address(request.getAddress())
                .cityFrom(request.getCityFrom())
                .cityTo(request.getCityTo())
                .cost(request.getCost())
                .dateOfCreation(request.getDateOfCreation())
                .dateOfPayment(request.getDateOfPayment())
                .departure(request.getDeparture())
                .receiving(request.getReceiving())
                .status(request.getStatus())
                .tariff(request.getTariff())
                .user(request.getUser())
                .build();
    }

    @Override
    public Request findById(Long id, String lang) {
        return requestDAO.findById(id, lang).orElse(null);
    }

    @Override
    public List<RequestForUserProfileDto> findAllUserRequests(Long id, String lang) {
        List<RequestForUserProfileDto> requestByUserDtos = new ArrayList<>();
        if (requestDAO.findAllUserRequests(id, lang) == null) {
            return null;
        }
        FillInList(requestByUserDtos, requestDAO.findAllUserRequests(id, lang));
        return requestByUserDtos;
    }

    @Override
    public List<RequestForUserProfileDto> findAllUserRequests(String lang) {
        List<RequestForUserProfileDto> requestByUserDtos = new ArrayList<>();
        if (requestDAO.findAllUserRequests(lang) == null) {
            return null;
        }
        FillInList(requestByUserDtos, requestDAO.findAllUserRequests(lang));
        return requestByUserDtos;
    }

    private void FillInList(List<RequestForUserProfileDto> requestByUserDtos, List<Request> allUserRequests) {
        allUserRequests.forEach(request -> requestByUserDtos.add(RequestForUserProfileDto.
                builder()
                .id(request.getId())
                .statusId(request.getStatus().getId())
                .cityFrom(request.getCityFrom().getName())
                .cityTo(request.getCityTo().getName())
                .departure(String.valueOf(request.getDeparture()).replace("T", " "))
                .receiving(String.valueOf(request.getReceiving()).replace("T", " "))
                .dateOfCreation(String.valueOf(request.getDateOfCreation()).replace("T", " "))
                .dateOfPayment(String.valueOf(request.getDateOfPayment()).replace("T", " "))
                .statusName(request.getStatus().getName())
                .cost(request.getCost())
                .address(request.getAddress())
                .build()));
    }

    @Override
    public List<RequestForManagerDto> findAllForManager(String lang) {
        List<RequestForManagerDto> requestForManagerDtos = new ArrayList<>();
        requestDAO.findAll(lang).stream().forEach(request -> requestForManagerDtos.add(RequestForManagerDto
                .builder()
                .id(request.getId())
                .address(request.getAddress())
                .cityFromName(request.getCityFrom().getName())
                .cityToName(request.getCityTo().getName())
                .cost(request.getCost())
                .dateOfCreation(String.valueOf(request.getDateOfCreation()).replace("T", " "))
                .departure(String.valueOf(request.getDeparture()).replace("T", " "))
                .receiving(String.valueOf(request.getReceiving()).replace("T", " "))
                .dateOfPayment(String.valueOf(request.getDateOfPayment()).replace("T", " "))
                .statusName(request.getStatus().getName())
                .tariffName(request.getTariff().getName())
                .userFirstName(request.getUser().getFirstName())
                .userId(request.getUser().getId())
                .build()));
        return requestForManagerDtos;
    }

    @Override
    public Boolean updateByManager(String departure, String receiving, String statusId, String requestId) {
        return requestDAO.updateByManager(new RequestUpdatedByManagerDto(
                Long.parseLong(requestId),
                LocalDateTime.parse(departure),
                LocalDateTime.parse(receiving),
                Long.parseLong(statusId)
        ));
    }


    @Override
    public Request createRequestByUser(RequestByUserDto requestByUserDto) {
        RequestInfoByUserDto reqInfo = requestByUserDto.getRequestInfoByUserDto();
        CargoDto cargoDto = requestByUserDto.getCargoDto();
        City cityFrom;
        City cityTo;
        Tariff tariff;
        List<Tariff> tariffs = reqInfo.getTariffs();
        List<City> cities = reqInfo.getCities();
        tariff = tariffs.stream()
                .filter(c -> c.getId() == Integer.parseInt(reqInfo.getTariff()))
                .findFirst().orElseThrow(RuntimeException::new);
        double tariffCost;
        if (tariff.getId() == 1L) {
            tariffCost = 35;
        } else {
            tariffCost = 65;
        }
        BigDecimal volume = new BigDecimal(cargoDto.getDepth()).multiply(new BigDecimal(cargoDto.getHeight())).multiply(new BigDecimal(cargoDto.getWidth()));
        BigDecimal d = volume.multiply(new BigDecimal(0.1)).add(new BigDecimal(cargoDto.getWeight()).divide(new BigDecimal(20)));
        String cost = d.add(new BigDecimal(tariffCost)).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString();
        cityFrom = cities.stream()
                .filter(c -> c.getId() == Integer.parseInt(reqInfo.getCityFrom()))
                .findFirst().orElseThrow(RuntimeException::new);
        cityTo = cities.stream()
                .filter(c -> c.getId() == Integer.parseInt(reqInfo.getCityTo()))
                .findFirst().orElseThrow(RuntimeException::new);
        Request request = Request
                .builder()
                .cityFrom(cityFrom)
                .cityTo(cityTo)
                .tariff(tariff)
                .address(reqInfo.getAddress())
                .cost(new BigDecimal(cost.replace(',', '.')))
                .user(reqInfo.getUser())
                .status(statusService.findById(1L, reqInfo.getLocale()))
                .dateOfCreation(LocalDateTime.now())
                .build();
        return requestDAO.createRequestByUser(request);
    }

    @Override
    public Boolean updateToPaidStatus(RequestUpdateStatusDto requestUpdateStatusDto) {
        return requestDAO.updateToPaidStatus(requestUpdateStatusDto);
    }

    @Override
    public List<Request> findAll(String lang) {
        return requestDAO.findAll(lang);
    }
}
