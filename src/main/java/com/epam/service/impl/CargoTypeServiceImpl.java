package com.epam.service.impl;

import com.epam.dao.impl.CargoTypeDAOImpl;
import com.epam.dao.interfaces.CargoTypeDAO;
import com.epam.entities.CargoType;
import com.epam.service.interfaces.CargoTypeService;

import java.util.List;

public class CargoTypeServiceImpl implements CargoTypeService {
    private CargoTypeDAO cargoTypeDAO = new CargoTypeDAOImpl();

    @Override
    public CargoType findById(Long id, String lang) {
        return cargoTypeDAO.findById(id, lang).orElseThrow(RuntimeException::new);
    }

    @Override
    public List<CargoType> findAll(String lang) {
        return cargoTypeDAO.findAll(lang);
    }

    @Override
    public CargoType create(String name) {
        CargoType cargoType = new CargoType();
        cargoType.setName(name);
        return cargoTypeDAO.create(cargoType);
    }

    @Override
    public void deleteById(Long id) {

    }
}
