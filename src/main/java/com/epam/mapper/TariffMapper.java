package com.epam.mapper;

import com.epam.entities.Tariff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TariffMapper implements ResultSetMapper<Tariff> {
    @Override
    public List<Tariff> map(ResultSet resultSet) {
        List<Tariff> tariffs = new ArrayList<>();
        try {
            while (resultSet.next()){
                Tariff tariff = new Tariff(
                        resultSet.getLong("id"),
                        resultSet.getString("name")
                );
                tariffs.add(tariff);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return tariffs;
    }
}
