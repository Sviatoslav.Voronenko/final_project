package com.epam.mapper;

import com.epam.entities.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class RequestMapper implements ResultSetMapper<Request> {
    @Override
    public List<Request> map(ResultSet resultSet) {
        List<Request> requests = new ArrayList<>();
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        try {
            while (resultSet.next()) {
                LocalDateTime departure = notNullDate(resultSet.getTimestamp("departure"));
                LocalDateTime receiving = notNullDate(resultSet.getTimestamp("receiving"));
                LocalDateTime dateOfCreation = notNullDate(resultSet.getTimestamp("date_of_creation"));
                LocalDateTime dateOfPayment = notNullDate(resultSet.getTimestamp("date_of_payment"));
                requests.add(Request.builder()
                        .id(resultSet.getLong("id"))
                        .departure(departure)
                        .receiving(receiving)
                        .cityTo(City.builder()
                                .id(resultSet.getLong("city_id_to"))
                                .name(resultSet.getString("city_to_name"))
                                .build())
                        .cityFrom(City.builder()
                                .id(resultSet.getLong("city_id_from"))
                                .name(resultSet.getString("city_from_name"))
                                .build())
                        .address(resultSet.getString("address"))
                        .status(new Status(
                                resultSet.getLong("status_id"),
                                resultSet.getString("status_name")
                        ))
                        .user(User.builder()
                                .id(resultSet.getLong("user_id"))
                                .firstName(resultSet.getString("first_name"))
                                .build())
                        .tariff(new Tariff(
                                resultSet.getLong("tariff_id"),
                                resultSet.getString("tariff_name")
                        ))
                        .cost(resultSet.getBigDecimal("cost"))
                        .dateOfCreation(dateOfCreation)
                        .dateOfPayment(dateOfPayment)
                        .build()
                );
            }
            return requests;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public LocalDateTime notNullDate(Timestamp timestamp) {
        if (timestamp != null) {
            return timestamp.toLocalDateTime();
        }
        return null;
    }
}
