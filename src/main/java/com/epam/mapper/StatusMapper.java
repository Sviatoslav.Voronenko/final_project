package com.epam.mapper;

import com.epam.entities.Status;
import com.epam.entities.Tariff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StatusMapper implements ResultSetMapper<Status> {
    @Override
    public List<Status> map(ResultSet resultSet) {
        List<Status> statuses = new ArrayList<>();
        try {
            while (resultSet.next()){
                Status status= new Status(
                        resultSet.getLong("id"),
                        resultSet.getString("name")
                );
                statuses.add(status);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return statuses;
    }
}
