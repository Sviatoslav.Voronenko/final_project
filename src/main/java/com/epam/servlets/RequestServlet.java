package com.epam.servlets;

import com.epam.dto.CargoDto;
import com.epam.dto.RequestForUserProfileDto;
import com.epam.dto.RequestUpdateStatusDto;
import com.epam.entities.Status;
import com.epam.service.impl.*;
import com.epam.service.interfaces.CargoService;
import com.epam.service.interfaces.RequestService;
import com.epam.service.interfaces.StatusService;
import com.epam.service.interfaces.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/request")
public class RequestServlet extends HttpServlet {

    private CargoService cargoService = new CargoServiceImpl();

    private RequestService requestService = new RequestServiceImpl();

    private UserService userService = new UserServiceImpl();

    private RoleServiceImpl roleService = new RoleServiceImpl();

    private StatusService statusService = new StatusServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String lang = session.getAttribute("lang").toString();
        String idParam = req.getParameter("id");
        System.out.println(idParam);
        if (idParam != null) {
            Long id = Long.valueOf(idParam);
            CargoDto cargo = cargoService.findByRequestId(id, lang);
            List<RequestForUserProfileDto> requestForUserProfileDtos = requestService.findAllUserRequests((String) session.getAttribute("lang"));
            RequestForUserProfileDto requestForUserProfileDto = requestForUserProfileDtos.stream().filter(r -> r.getId() == id).findFirst().orElse(null);
            List<Status> statuses = statusService.findAll(lang);
            System.out.println("forSolo");
            System.out.println(requestService.findByIdAll(id, lang));
            req.setAttribute("statuses", statuses);
            req.setAttribute("request", requestForUserProfileDto);
            req.setAttribute("cargo", cargo);
            req.getRequestDispatcher("WEB-INF/jsp/request.jsp").forward(req, resp);
        } else {
            System.out.println("forList");
            List<RequestForUserProfileDto> requestForUserProfileDtos = requestService.findAllUserRequests((Long) session.getAttribute("userId"), (String) session.getAttribute("lang"));
            req.setAttribute("requests", requestForUserProfileDtos);
            List<Status> statuses = statusService.findAll(lang);
            req.setAttribute("statuses", statuses);
            req.getRequestDispatcher("WEB-INF/jsp/requests.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String reqId = req.getParameter("request_id");
        System.out.println(reqId);
        RequestUpdateStatusDto requestUpdateStatusDto = new RequestUpdateStatusDto();
        if (reqId != null) {
            requestUpdateStatusDto.setRequestId(Long.parseLong(reqId));
        } else {
            throw new NullPointerException();
        }
        System.out.println(requestUpdateStatusDto);
        requestService.updateToPaidStatus(requestUpdateStatusDto);
        resp.sendRedirect("/orderComplete");
    }
}
