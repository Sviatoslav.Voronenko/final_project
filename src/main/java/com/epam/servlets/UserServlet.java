package com.epam.servlets;

import com.epam.entities.User;
import com.epam.service.impl.UserServiceImpl;
import com.epam.service.interfaces.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/user")
public class UserServlet extends HttpServlet {

    private UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id != null) {
            User user = userService.findById(Long.parseLong(id));
            req.setAttribute("userProfile", user);
            req.getRequestDispatcher("WEB-INF/jsp/admin/user.jsp").forward(req, resp);
            return;
        } else {
            req.getRequestDispatcher("WEB-INF/jsp/admin/orderList.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
