package com.epam.servlets;

import com.epam.entities.RoleName;
import com.epam.entities.User;
import com.epam.service.impl.UserServiceImpl;
import com.epam.service.interfaces.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(LoginServlet.class);

    private UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("LoginServlet starts");
        PrintWriter out = resp.getWriter();


        String email = req.getParameter("name");
        LOG.trace("Request parameter: login --> " + email);

        String password = req.getParameter("password");

        if (email == null || password == null || email.isEmpty() || password.isEmpty()) {
            throw new IOException("Login/password cannot be empty");
        }

        User user = userService.findByEmail(email);
        LOG.trace("Found in DB: user --> " + user);


        if (user != null && user.getEmail().equals(email) && user.getPassword().equals(password)) {
            HttpSession session = req.getSession();
            String userRole = user.getRole().getName();
            session.setAttribute("login", email);
            session.setAttribute("firstName", user.getFirstName());
            session.setAttribute("user", user);
            session.setAttribute("userId", user.getId());

            LOG.trace("Set the session attribute: user --> " + user);
            session.setAttribute("userRole", userRole);

            LOG.trace("Set the session attribute: userRole --> " + userRole);
            session.setAttribute("lang", "ru");

            session.setMaxInactiveInterval(30 * 60);
            LOG.info("User " + user + " logged as " + userRole.toLowerCase());
            if (user.getRole().getName().equals(RoleName.ADMIN.getName())) {
                session.setAttribute("isAdmin", RoleName.ADMIN.getName());
                LOG.trace("Set the session attribute: isAdmin --> true");
            }

            LOG.debug("LoginServlet finished");

            resp.sendRedirect("/home");
        } else {
            req.getRequestDispatcher("WEB-INF/jsp/index.jsp");
            out.print("wrong login or password");
        }
        out.close();
    }
}
