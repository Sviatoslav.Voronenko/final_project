package com.epam.servlets;

import com.epam.entities.City;
import com.epam.entities.User;
import com.epam.service.impl.CityServiceImpl;
import com.epam.service.impl.RoleServiceImpl;
import com.epam.service.impl.UserServiceImpl;
import com.epam.service.interfaces.CityService;
import com.epam.service.interfaces.RoleService;
import com.epam.service.interfaces.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/adminPanel")
public class AdminListServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(AdminListServlet.class);

    private RoleService roleService = new RoleServiceImpl();

    private UserService userService = new UserServiceImpl();

    private CityService cityService = new CityServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String lang = session.getAttribute("lang").toString();
        List<User> users = userService.findAll();
        req.setAttribute("users", users);
        List<City> cities = cityService.findAll(lang);
        req.setAttribute("cities", cities);
        req.getRequestDispatcher("WEB-INF/jsp/admin/list.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
