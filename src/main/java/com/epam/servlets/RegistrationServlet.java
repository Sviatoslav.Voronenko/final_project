package com.epam.servlets;

import com.epam.entities.User;
import com.epam.service.impl.UserServiceImpl;
import com.epam.service.interfaces.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(RegistrationServlet.class);

    private final UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/jsp/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("name");
        String email = req.getParameter("email");
        String phone = req.getParameter("phone");
        String password = req.getParameter("password");
        if (userService.findByEmail(email) != null) {
            resp.setStatus(1100);
            return;
        }
        if (firstName != null && email != null && phone != null && password != null) {
            User user = User.builder()
                    .firstName(firstName)
                    .email(email)
                    .phone(phone)
                    .password(password)
                    .build();
            userService.create(user);
        }
    }
}
