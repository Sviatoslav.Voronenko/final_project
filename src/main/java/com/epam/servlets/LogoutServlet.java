package com.epam.servlets;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(LogoutServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("Logout starts");
        resp.setContentType("text/html");
        HttpSession session = req.getSession(false);
        String lang = "ru";
        if (session != null) {
            lang = session.getAttribute("lang").toString();
            LOG.debug("Session " + session.getId() + " is over");
            session.invalidate();
        }
        req.setAttribute("lang", lang);
        req.getRequestDispatcher("WEB-INF/jsp/home.jsp").forward(req, resp);
    }
}
