package com.epam.servlets;

import com.epam.dto.CargoDto;
import com.epam.dto.RequestByUserDto;
import com.epam.dto.RequestInfoByUserDto;
import com.epam.entities.*;
import com.epam.service.impl.*;
import com.epam.service.interfaces.*;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet("/order")
public class OrderServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(OrderServlet.class);

    private RequestByUserDto requestByUserDto = new RequestByUserDto();

    private CityService cityService = new CityServiceImpl();

    private TariffService tariffService = new TariffServiceImpl();

    private CargoTypeService cargoTypeService = new CargoTypeServiceImpl();

    private RequestService requestService = new RequestServiceImpl();

    private CargoService cargoService = new CargoServiceImpl();

    private List<Tariff> tariffs;

    private List<City> cities;

    private List<CargoType> cargoTypes;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        cities = cityService.findAll(session.getAttribute("lang").toString());
        String lang = session.getAttribute("lang").toString();
        cargoTypes = cargoTypeService.findAll(lang);
        req.setAttribute("cargotypes", cargoTypes);
        req.setAttribute("cities", cities);
        tariffs = tariffService.findAll(lang);
        req.setAttribute("tariffs", tariffs);
        req.getRequestDispatcher("WEB-INF/jsp/order.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        req.setCharacterEncoding("UTF-8");
        requestByUserDto.setRequestInfoByUserDto(RequestInfoByUserDto
                .builder()
                .cityFrom(req.getParameter("cityFrom"))
                .cityTo(req.getParameter("cityTo"))
                .tariff(req.getParameter("tariff"))
                .address(req.getParameter("address"))
                .cost(req.getParameter("cost"))
                .cities(cities)
                .tariffs(tariffs)
                .cargoTypes(cargoTypes)
                .user((User) session.getAttribute("user"))
                .locale(session.getAttribute("lang").toString())
                .build());
        requestByUserDto.setCargoDto(CargoDto
                .builder()
                .depth(Integer.parseInt(req.getParameter("depth")))
                .width(Integer.parseInt(req.getParameter("width")))
                .height(Integer.parseInt(req.getParameter("height")))
                .weight(Integer.parseInt(req.getParameter("weight")))
                .cargoType(cargoTypes.stream()
                        .filter(c -> c.getName().equals(req.getParameter("cargoType")))
                        .findFirst().orElse(null))
                .build());
        Request request = requestService.createRequestByUser(requestByUserDto);
        requestByUserDto.getCargoDto().setRequestId(request);
        cargoService.create(requestByUserDto.getCargoDto());
        req.getRequestDispatcher("WEB-INF/jsp/ThanksForOrder.jsp").forward(req, resp);
    }
}
