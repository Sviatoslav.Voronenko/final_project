package com.epam.servlets;

import com.epam.dto.UserInfoForUserDto;
import com.epam.entities.User;
import com.epam.service.impl.RequestServiceImpl;
import com.epam.service.impl.RoleServiceImpl;
import com.epam.service.impl.UserServiceImpl;
import com.epam.service.interfaces.RequestService;
import com.epam.service.interfaces.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
    private final static Logger LOG = Logger.getLogger(ProfileServlet.class);

    private RequestService requestService = new RequestServiceImpl();

    private UserService userService = new UserServiceImpl();

    private RoleServiceImpl roleService = new RoleServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User user = userService.findByEmail(String.valueOf(session.getAttribute("login")));
        UserInfoForUserDto userInfoForUserDto = UserInfoForUserDto
                .builder()
                .first_name(user.getFirstName())
                .last_name(user.getLastName())
                .email(user.getEmail())
                .phone(user.getPhone())
                .build();
        req.setAttribute("user", userInfoForUserDto);
        req.getRequestDispatcher("WEB-INF/jsp/profile.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
