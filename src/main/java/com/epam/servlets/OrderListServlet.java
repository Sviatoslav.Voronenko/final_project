package com.epam.servlets;


import com.epam.dto.RequestForManagerDto;
import com.epam.entities.Status;
import com.epam.service.impl.RequestServiceImpl;
import com.epam.service.impl.StatusServiceImpl;
import com.epam.service.interfaces.RequestService;
import com.epam.service.interfaces.StatusService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/orderList")
public class OrderListServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(OrderServlet.class);

    private RequestService requestService = new RequestServiceImpl();

    private StatusService statusService = new StatusServiceImpl();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String lang = session.getAttribute("lang").toString();
        List<RequestForManagerDto> requests = requestService.findAllForManager(lang);
        List<Status> statuses = statusService.findAll(lang);
        req.setAttribute("requests", requests);
        req.setAttribute("statuses", statuses);
        req.getRequestDispatcher("WEB-INF/jsp/admin/orderList.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        String departure = req.getParameter("departure_date_new");
        String receiving = req.getParameter("receiving_date_new");
        String statusName = req.getParameter("status");
        String requestId = req.getParameter("requestId");
        String lang = session.getAttribute("lang").toString();
        if (departure.equals("")) {
            departure = req.getParameter("oldDeparture").replace(" ", "T");
        }
        if (receiving.equals("")) {
            receiving = req.getParameter("oldReceiving").replace(" ", "T");
        }
        List<Status> statuses = statusService.findAll(lang);
        String statusId = String.valueOf(statuses.stream().filter(s -> s.getName().equals(statusName)).findFirst().orElse(null).getId());
        requestService.updateByManager(departure, receiving, statusId, requestId);
        resp.sendRedirect("/orderList");
    }
}
