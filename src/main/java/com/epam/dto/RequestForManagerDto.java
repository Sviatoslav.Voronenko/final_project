package com.epam.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestForManagerDto {
    private Long id;
    private String departure;
    private String receiving;
    private String cityToName;
    private String cityFromName;
    private String address;
    private String statusName;
    private String dateOfCreation;
    private Long userId;
    private String userFirstName;
    private String tariffName;
    private BigDecimal cost;
    private String dateOfPayment;
}
