package com.epam.dto;


import com.epam.entities.City;
import com.epam.entities.Status;
import com.epam.entities.Tariff;
import com.epam.entities.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestDto {
    private Long id;
    private LocalDateTime departure;
    private LocalDateTime receiving;
    private City cityTo;
    private City cityFrom;
    private String address;
    private Status status;
    private LocalDateTime dateOfCreation;
    private User user;
    private Tariff tariff;
    private BigDecimal cost;
    private LocalDateTime dateOfPayment;

    public String getDeparture() {
        return String.valueOf(departure).replace("T", " ");
    }

    public String getReceiving() {
        return String.valueOf(departure).replace("T", " ");
    }

    public String getCityTo() {
        return cityTo.getName();
    }

    public String getCityFrom() {
        return cityFrom.getName();
    }

    public String getDateOfCreation() {
        return String.valueOf(departure).replace("T", " ");
    }

    public String getTariff() {
        return tariff.getName();
    }

    public String getDateOfPayment() {
        return String.valueOf(departure).replace("T", " ");
    }
}
