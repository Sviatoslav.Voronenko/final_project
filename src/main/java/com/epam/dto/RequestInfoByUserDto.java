package com.epam.dto;

import com.epam.entities.CargoType;
import com.epam.entities.City;
import com.epam.entities.Tariff;
import com.epam.entities.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestInfoByUserDto {
    String cityFrom;
    String cityTo;
    String tariff;
    String address;
    String cost;
    List<City> cities;
    List<Tariff> tariffs;
    List<CargoType> cargoTypes;
    User user;
    String locale;
}
