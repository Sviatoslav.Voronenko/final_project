package com.epam.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestForUserProfileDto {
    private Long id;
    private String departure;
    private String receiving;
    private String address;
    private String statusName;
    private Long statusId;
    private String cityTo;
    private String cityFrom;
    private String dateOfCreation;
    private String dateOfPayment;
    private BigDecimal cost;
}
