package com.epam.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestCargoInfoByUserDto {
    String depth;
    String width;
    String height;
    String weight;
    String cargoType;
}
