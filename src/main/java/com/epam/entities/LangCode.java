package com.epam.entities;

public enum LangCode {
    EN("en"), RU("ru");

    private final String name;

    LangCode(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
