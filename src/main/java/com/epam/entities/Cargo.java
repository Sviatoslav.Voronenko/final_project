package com.epam.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Cargo {
    private Long id;
    private int weight;
    private int height;
    private int width;
    private int depth;
    private CargoType cargoType;
    private Request requestId;
}
