package com.epam.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private Long id;
    private LocalDateTime departure;
    private LocalDateTime receiving;
    private City cityTo;
    private City cityFrom;
    private String address;
    private Status status;
    private LocalDateTime dateOfCreation;
    private User user;
    private Tariff tariff;
    private BigDecimal cost;
    private LocalDateTime dateOfPayment;
}
