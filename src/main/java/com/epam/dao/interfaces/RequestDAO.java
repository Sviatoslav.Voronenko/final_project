package com.epam.dao.interfaces;

import com.epam.dto.RequestUpdateStatusDto;
import com.epam.dto.RequestUpdatedByManagerDto;
import com.epam.entities.Request;

import java.util.List;

public interface RequestDAO extends GenericDAOTranslator<Request, Long> {
    Request createRequestByUser(Request request);

    List<Request> findAllUserRequests(Long id, String lang);

    List<Request> findAllUserRequests(String lang);

    Boolean updateByManager(RequestUpdatedByManagerDto requestUpdatedByManagerDto);

    Boolean updateToPaidStatus(RequestUpdateStatusDto updateStatus);

}
