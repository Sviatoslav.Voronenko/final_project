package com.epam.dao.interfaces;

import com.epam.entities.Cargo;

import java.util.Optional;

public interface CargoDAO extends GenericDAOTranslator<Cargo, Long> {
    Optional<Cargo> findByRequestId(Long id, String lang);
}
