package com.epam.dao.interfaces;

import com.epam.entities.City;

public interface CityDAO extends GenericDAOTranslator<City, Long> {
}
