package com.epam.dao.interfaces;

import com.epam.entities.CargoType;

public interface CargoTypeDAO extends GenericDAOTranslator<CargoType, Long> {
}
