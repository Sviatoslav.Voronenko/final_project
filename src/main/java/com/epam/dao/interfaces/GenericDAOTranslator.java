package com.epam.dao.interfaces;

import java.util.List;
import java.util.Optional;

public interface GenericDAOTranslator<T, ID> {
    Optional<T> findById(ID id, String lang);

    List<T> findAll(String lang);

    List<T> findAllTranslate(String lang);

    T create(T entity);

    T createTranslate(T entity, String lang);

    T update(T entity, ID id, String lang);

    void deleteById(ID id);

    void delete(T entity);
}
