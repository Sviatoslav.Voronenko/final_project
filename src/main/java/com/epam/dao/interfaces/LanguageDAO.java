package com.epam.dao.interfaces;

import java.util.List;

public interface LanguageDAO {
    Long getIdByLangCode(String lang);

    List<String> findAllLangCodes();
}
