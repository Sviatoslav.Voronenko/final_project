package com.epam.dao.interfaces;

import com.epam.entities.Status;

public interface StatusDAO extends GenericDAOTranslator<Status, Long> {
}
