package com.epam.dao.interfaces;

import java.util.List;
import java.util.Optional;

public interface GenericDAO<T, ID> {

    Optional<T> findById(ID id);

    List<T> findAll();

    T create(T entity);

    T update(T entity, ID id);

    void deleteById(ID id);

    void delete(T entity);
}
