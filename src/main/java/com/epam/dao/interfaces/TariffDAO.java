package com.epam.dao.interfaces;

import com.epam.entities.Tariff;

public interface TariffDAO extends GenericDAOTranslator<Tariff, Long>{
}
