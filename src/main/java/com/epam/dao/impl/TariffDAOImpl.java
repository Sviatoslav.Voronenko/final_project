package com.epam.dao.impl;

import com.epam.dao.interfaces.TariffDAO;
import com.epam.entities.LangCode;
import com.epam.entities.Tariff;
import com.epam.mapper.TariffMapper;
import com.epam.util.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

import static com.epam.queries.TariffSqlQueries.*;

public class TariffDAOImpl extends AbstractDAO<Tariff> implements TariffDAO {
    private static final Logger LOG = Logger.getLogger(TariffDAOImpl.class);

    private Connection connection;

    public TariffDAOImpl() {
        this.connection = ConnectionPool.getConnection();
        super.mapper = new TariffMapper();
    }

    @Override
    public Optional<Tariff> findById(Long id, String lang) {
        String sql;
        if (lang.equals(LangCode.RU.getName())) {
            sql = SELECT_FROM_TARIFF_WHERE_ID;
        } else {
            sql = SELECT_FROM_TARIFF_TRANSLATE;
        }
        return super.findById(id, lang, sql);
    }

    @Override
    public List<Tariff> findAll(String lang) {
        if (!lang.equals(LangCode.RU.getName())) {
            return findAllTranslate(lang);
        }
        String sql = SELECT_FROM_TARIFF;
        return super.findAll(lang, sql);
    }

    @Override
    public List<Tariff> findAllTranslate(String lang) {
        String sql = SELECT_FROM_TARIFFS_TRANSLATE;
        return super.findAllTranslate(lang, sql);
    }

    @Override
    public Tariff create(Tariff entity) {
        String sql = INSERT_INTO_TARIFF;
        entity.setId(super.create(entity.getName(), sql));
        return entity;
    }

    @Override
    public Tariff createTranslate(Tariff entity, String lang) {
        return null;
    }

    @Override
    public Tariff update(Tariff entity, Long id, String lang) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void delete(Tariff entity) {

    }
}
