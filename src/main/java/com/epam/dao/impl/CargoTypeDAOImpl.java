package com.epam.dao.impl;

import com.epam.dao.interfaces.CargoTypeDAO;
import com.epam.entities.CargoType;
import com.epam.entities.LangCode;
import com.epam.mapper.CargoTypeMapper;
import com.epam.util.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

import static com.epam.queries.CargoTypeSqlQueries.*;

public class CargoTypeDAOImpl extends AbstractDAO<CargoType> implements CargoTypeDAO {

    private static final Logger LOG = Logger.getLogger(CargoTypeDAOImpl.class);

    private Connection connection;

    public CargoTypeDAOImpl() {
        this.connection = ConnectionPool.getConnection();
        super.mapper = new CargoTypeMapper();
    }

    @Override
    public Optional<CargoType> findById(Long id, String lang) {
        String sql;
        if (lang.equals(LangCode.RU.getName())) {
            sql = SELECT_FROM_CARGOTYPE_WHERE_ID;
        } else {
            sql = SELECT_FROM_CARGOTYPE_TRANSLATE;
        }
        return super.findById(id, lang, sql);
    }

    @Override
    public List<CargoType> findAll(String lang) {
        if (!lang.equals(LangCode.RU.getName())) {
            return findAllTranslate(lang);
        }
        String sql = SELECT_FROM_CARGOTYPE;
        return super.findAll(lang, sql);
    }

    @Override
    public List<CargoType> findAllTranslate(String lang) {
        String sql = SELECT_FROM_CARGOTYPES_TRANSLATE;
        return super.findAllTranslate(lang, sql);
    }

    @Override
    public CargoType create(CargoType entity) {
        String sql = INSERT_INTO_CARGOTYPE_NAME_VALUES;
        entity.setId(super.create(entity.getName(), sql));
        return entity;
    }

    @Override
    public CargoType createTranslate(CargoType entity, String lang) {
        return null;
    }

    @Override
    public CargoType update(CargoType entity, Long aLong, String lang) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(CargoType entity) {

    }
}
