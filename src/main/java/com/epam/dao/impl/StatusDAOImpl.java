package com.epam.dao.impl;

import com.epam.dao.interfaces.StatusDAO;
import com.epam.entities.LangCode;
import com.epam.entities.Status;
import com.epam.mapper.StatusMapper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

import static com.epam.queries.StatusSqlQueries.*;

public class StatusDAOImpl extends AbstractDAO<Status> implements StatusDAO {

    private static final Logger LOG = Logger.getLogger(StatusDAOImpl.class);

    private Connection connection;

    public StatusDAOImpl() {
        this.connection = connection;
        super.mapper = new StatusMapper();
    }

    @Override
    public Optional<Status> findById(Long id, String lang) {
        String sql;
        if (lang.equals(LangCode.RU.getName())) {
            sql = SELECT_FROM_STATUS_WHERE_ID;
        } else {
            sql = SELECT_FROM_STATUS_TRANSLATE;
        }
        return super.findById(id, lang, sql);
    }


    @Override
    public List<Status> findAll(String lang) {
        if (!lang.equals(LangCode.RU.getName())) {
            return findAllTranslate(lang);
        }
        String sql = SELECT_FROM_STATUS;
        return super.findAll(lang, sql);
    }

    @Override
    public List<Status> findAllTranslate(String lang) {
        String sql = SELECT_ALL_FROM_STATUSES_TRANSLATE;
        return super.findAllTranslate(lang, sql);
    }

    @Override
    public Status create(Status entity) {
        String sql = INSERT_INTO_STATUS_NAME_VALUES;
        entity.setId(super.create(entity.getName(), sql));
        return entity;
    }

    @Override
    public Status createTranslate(Status entity, String lang) {
        return null;
    }

    @Override
    public Status update(Status entity, Long id, String lang) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void delete(Status entity) {

    }
}
