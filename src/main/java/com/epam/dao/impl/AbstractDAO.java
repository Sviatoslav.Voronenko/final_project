package com.epam.dao.impl;

import com.epam.entities.LangCode;
import com.epam.mapper.ResultSetMapper;
import com.epam.util.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.List;
import java.util.Optional;

public class AbstractDAO<T> {

    private static final Logger LOG = Logger.getLogger(AbstractDAO.class);

    public static final String SELECT_LAST_INSERT_ID_AS_ID = "select last_insert_id() as id";
    protected Connection connection;
    protected ResultSetMapper<T> mapper;

    public AbstractDAO() {
        this.connection = ConnectionPool.getConnection();
    }

    protected Long getLastInsertId() {
        String sql = SELECT_LAST_INSERT_ID_AS_ID;
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            if (resultSet.next()) {
                return resultSet.getLong("id");
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected Optional<T> findById(Long id,String lang ,String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            if (!lang.equals(LangCode.RU.getName())) preparedStatement.setString(2, lang);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> res = mapper.map(resultSet);
            if (res.size() > 1) {
                throw new SQLException("Received more than one record.");
            }
            return res.stream().findFirst();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    protected Optional<T> findById(Long id, String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> res = mapper.map(resultSet);
            if (res.size() > 1) {
                throw new SQLException("Received more than one record.");
            }
            return res.stream().findFirst();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<T> findAllById(Long id, String lang, String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            if (!lang.equals(LangCode.RU.getName())) preparedStatement.setString(2, lang);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> res = mapper.map(resultSet);
            if (res.size() < 1) {
                return null;
            }
            return res;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected Optional<T> findByName(String name, String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> res = mapper.map(resultSet);
            if (res.size() > 1) {
                throw new SQLException("Received more than one record.");
            }
            return res.stream().findFirst();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<T> findAll(String lang, String sql) {
        if (!lang.equals(LangCode.RU.getName())) {
            return findAllTranslate(lang, sql);
        }
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            return mapper.map(resultSet);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<T> findAll(String sql) {
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            return mapper.map(resultSet);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<T> findAllTranslate(String lang, String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, lang);
            ResultSet resultSet = preparedStatement.executeQuery();
            return mapper.map(resultSet);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected Long create(String name, String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();
            return getLastInsertId();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
