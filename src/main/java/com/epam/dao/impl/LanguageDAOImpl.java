package com.epam.dao.impl;

import com.epam.dao.interfaces.LanguageDAO;
import com.epam.util.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LanguageDAOImpl implements LanguageDAO {

    private static final String FIND_ID_BY_NAME = "select id from languages where lang_code = ?";
    private static final String SELECT_LANG_CODE_AS_LANG_FROM_LANGUAGES = "select lang_code as lang from languages";

    @Override
    public List<String> findAllLangCodes() {
        String sql = SELECT_LANG_CODE_AS_LANG_FROM_LANGUAGES;
        List<String> langs = new ArrayList<>();
        try(Connection connection = ConnectionPool.getConnection();
            Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                langs.add(resultSet.getString("lang"));
            }
            return langs;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public Long getIdByLangCode(String lang) {
        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_ID_BY_NAME)) {
            preparedStatement.setString(1, lang);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return resultSet.getLong("id");
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
