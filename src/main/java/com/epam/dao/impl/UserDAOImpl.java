package com.epam.dao.impl;

import com.epam.dao.interfaces.RoleDAO;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entities.User;
import com.epam.mapper.UserMapper;
import com.epam.util.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static com.epam.queries.UserSqlQueries.*;

public class UserDAOImpl extends AbstractDAO<User> implements UserDAO {

    private static final Logger LOG = Logger.getLogger(UserDAOImpl.class);

    private Connection connection;

    private RoleDAO roleDAO = new RoleDAOImpl();

    public UserDAOImpl() {
        this.connection = ConnectionPool.getConnection();
        super.mapper = new UserMapper();
    }

    @Override
    public Optional<User> findById(Long id) {
        String sql = SELECT_FROM_USER_BY_ID;
        return super.findById(id, sql);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        String sql = SELECT_FROM_USER_BY_EMAIL;
        return super.findByName(email, sql);
    }

    @Override
    public List<User> findAll() {
        String sql = SELECT_ALL_FROM_USER;
        return super.findAll(sql);
    }

    @Override
    public User create(User entity) {
        String sql = INSERT_INTO_USER_FIRST_NAME_EMAIL_PASSWORD_PHONE_ROLE_ID_VALUES;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getEmail());
            preparedStatement.setString(3, entity.getPassword());
            preparedStatement.setString(4, entity.getPhone());
            preparedStatement.setLong(5, roleDAO.findByName("user").orElseThrow(RuntimeException::new).getId());
            preparedStatement.executeUpdate();
            entity.setId(getLastInsertId());
            return entity;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User update(User entity, Long id) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(User entity) {

    }
}
