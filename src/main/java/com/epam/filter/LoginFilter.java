package com.epam.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter({"/orderList", "/profile", "/request", "/adminPanel"})
public class LoginFilter implements Filter {
    public static final Logger LOG = Logger.getLogger(LoginFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOG.debug("Filter initialization starts");

        LOG.debug("Filter initialization finished");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        LOG.debug("Filter starts");
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        String loginURI = request.getContextPath() + "/login";

        boolean loggedIn = session != null && session.getAttribute("login") != null && session.getAttribute("userRole") != null;
        boolean loginRequest = request.getRequestURI().equals(loginURI);
        LOG.debug("User logged in -> " + loggedIn);
        if (loggedIn || loginRequest) {
            filterChain.doFilter(request, response);
        } else {
            response.sendRedirect(loginURI);
        }
        LOG.debug("Filter finished");
    }

    @Override
    public void destroy() {
        LOG.debug("Filter destruction starts");

        LOG.debug("Filter destruction finished");
    }
}
